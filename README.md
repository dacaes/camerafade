# CameraFade

![screenshot](ReadmeData/screenshot.png)

The simplest way to fade in Unity3D!
Just add CameraFade component to the camera and call CameraFade.In and CameraFade.Out from anywhere using the namespace CameraFading.

No shader needed.
Demo included.

Created by **Daniel Castaño Estrella**.

Contact: daniel.c.estrella@gmail.com

License in LICENSE file.